import React, { Fragment } from "react";
import { render } from "react-dom";

import "./styles/video.css";
import "./styles/overlay.css";

import Overlay from "./components/Overlay";
import Video from "./components/Video";

const video = "http://clips.vorwaerts-gmbh.de/VfE_html5.mp4";

function OverlayContent() {
  return (
    <Fragment>
      <div>
        <p>Overlay Content</p>
      </div>
      <div>
        <button>Button</button>
      </div>
    </Fragment>
  );
}

render(
  <Fragment>
    <div>
      <h3>
        Misunderstood the assignment and made this first. ( Also the reason for
        the delay in submitting )
      </h3>
      <Video src={video} />
    </div>
    <div>
      <h3>Solution for the asked assignment</h3>
      <Overlay videoSrc={video}>
        <OverlayContent />
      </Overlay>
    </div>
  </Fragment>,
  document.getElementById("app")
);
