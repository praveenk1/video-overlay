import React from "react";

function Overlay({ videoSrc, children }) {
  return (
    <div className="wrapper">
      <div className="overlay">{children}</div>
      <video autoPlay controls={false} src={videoSrc} />
    </div>
  );
}

export default Overlay;
