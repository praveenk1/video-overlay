import React, { Component, createRef, Fragment } from "react";

import {
  FiPlay,
  FiPause,
  FiVolume2,
  FiChevronLeft,
  FiChevronRight
} from "react-icons/fi";

class Video extends Component {
  constructor(props) {
    super(props);
    this.videoRef = createRef();
    this.state = {
      showControls: false,
      isPlaying: false,
      currentTime: 0,
      playedDuration: 0,
      volume: "1"
    };
    this.showControls = this.showControls.bind(this);
    this.hideControls = this.hideControls.bind(this);
    this.play = this.play.bind(this);
    this.pause = this.pause.bind(this);
    this.setVolume = this.setVolume.bind(this);
    this.seekTo = this.seekTo.bind(this);
    this._forward = this._forward.bind(this);
    this._backward = this._backward.bind(this);
  }

  componentDidMount() {
    this.video = this.videoRef.current;
    this.showControls();
    [("ended", "pause")].forEach(e =>
      this.video.addEventListener(e, () => this.setState({ isPlaying: false }))
    );
    ["seeked", "timeupdate"].forEach(e =>
      this.video.addEventListener(e, () =>
        this.setState({ currentTime: this.video.currentTime })
      )
    );
    this.video.addEventListener("volumechange", e =>
      this.setState({ volume: e.target.volume })
    );
    this.video.addEventListener("playing", () =>
      this.setState({ isPlaying: true })
    );
  }

  setVolume(e) {
    this.video.volume = e.target.value;
  }

  play() {
    this.video.play();
  }

  seekTo(e) {
    this.video.currentTime = e.target.value * this.video.duration;
  }

  pause() {
    this.video.pause();
  }

  showControls() {
    this.setState({ showControls: true });
  }

  hideControls() {
    this.setState({ showControls: false });
  }

  _playedDuration() {
    return this.state.currentTime === 0
      ? 0
      : this.state.currentTime / this.video.duration;
  }

  _forward() {
    this.video.currentTime = this.video.currentTime + 5;
  }

  _backward() {
    this.video.currentTime = this.video.currentTime - 5;
  }

  _overlay() {
    const { isPlaying, volume } = this.state;
    return (
      this.video && (
        <div className="overlay">
          {isPlaying ? (
            <FiPause onClick={this.pause} size={32} />
          ) : (
            <FiPlay onClick={this.play} size={32} />
          )}
          <FiChevronLeft onClick={this._backward} size={32} />
          <h3>{Math.floor(this.video.currentTime)}</h3>
          <input
            type="range"
            min={0}
            max={1}
            step={0.01}
            value={this._playedDuration()}
            onChange={this.seekTo}
          />
          <h3>{Math.floor(this.video.duration) || "-:-"}</h3>
          <FiChevronRight onClick={this._forward} size={32} />
          <FiVolume2 size={32} />
          <input
            type="range"
            min={0}
            max={1}
            step={0.01}
            value={volume}
            onChange={this.setVolume}
          />
        </div>
      )
    );
  }

  render() {
    const { showControls } = this.state;
    return (
      <div className="player">
        <video ref={this.videoRef} controls={false} {...this.props} />
        {showControls && this._overlay()}
      </div>
    );
  }
}

export default Video;
